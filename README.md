# ics-ans-role-rems-crop

Ansible role to install rems-crop.

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-rems-crop
```

## License

BSD 2-clause
